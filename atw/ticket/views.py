from django.shortcuts import get_object_or_404, render
from django.views import View
from django.conf.urls import handler404, handler500
from ticket.models import TicketRep, Proyecto

from datetime import timedelta

class TicketStatus(View):

    template_name = 'ticket/payment_status.html'

    def get(self, request, urlHash):
        ticket = get_object_or_404(
            TicketRep,
            urlhash=urlHash)

        ticket.qr_uri = 'verge:' + ticket.address_rec
        
        ticket.limitToPay = ticket.momento + timedelta(hours=24)
        
        proyecto = Proyecto.objects.get(proyecto_id=ticket.proyecto_id)        
                
        return render(
            request,
            self.template_name,
            {'ticket': ticket, 'proyecto': proyecto})


