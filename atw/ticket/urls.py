from django.conf.urls import url

from .views import TicketStatus

urlpatterns = [
    url(r'^(?P<urlHash>[A-Fa-f0-9]{16})$', TicketStatus.as_view())
]
