from django.contrib import admin
from django.conf import settings
from django.conf.urls.static import static
from django.urls import include, path

from ticket import urls as ticket_urls


urlpatterns = [
    path('admin/', admin.site.urls),
    path('ticket/', include(ticket_urls)),
] + static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
