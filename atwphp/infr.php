<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>ATW</title>
        <link rel="stylesheet" href="/media/css/materialize.min.css">
        <link rel="stylesheet" href="/media/css/main.css">
        <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
        <script>var countDownDate = Date.UTC(<?php print substr($fecha, 0, 4).",".
                            (intval(substr($fecha, 5, 2))-1).",".
                            substr($fecha, 8, 2).",".substr($fecha, 11, 2).",".
                            substr($fecha, 14, 2).",".  substr($fecha, 17, 2); ?>);</script>
        <script src="/media/fs.js"></script>
    </head>
    <body>
        <nav>
    <div class="nav-wrapper container">
      <a href="#!" class="brand-logo">Logo</a>
      <a href="#" data-target="mobile-demo" class="sidenav-trigger"><i class="material-icons">menu</i></a>
      <ul class="right hide-on-med-and-down">
        <li><a href="/index.php">Index</a></li>
        <li><a href="/infr.php">Income</a></li>
      </ul>
    </div>
  </nav>

  <ul class="sidenav" id="mobile-demo">
        <li><a href="/index.php">Index</a></li>
        <li><a href="/infr.php">Income</a></li>
  </ul>

        <div class="container">
            <article>
                <h2>Income</h2>
                <?php
                include ("db.php");
                if (array_key_exists("WinnerWallet", $_REQUEST)) {
        /*            $post_data = http_build_query(
                            array(
                                'secret' => "6Ldbc1IUAAAAACKbWD6ylql2Z9F6r5xt2bo2kBwX",
                                'response' => $_POST['g-recaptcha-response'],
                                'remoteip' => $_SERVER['REMOTE_ADDR']
                            )
                    );
                    $opts = array('http' =>
                        array(
                            'method' => 'POST',
                            'header' => 'Content-type: application/x-www-form-urlencoded',
                            'content' => $post_data
                        )
                    );
                    $context = stream_context_create($opts);
                    $response = file_get_contents('https://www.google.com/recaptcha/api/siteverify', false, $context);
                    $result = json_decode($response);
                    if ($result->success) { */
                        $iWaWi = str_replace(" ", "", str_replace("=", "", trim(filter_input(INPUT_POST, "WinnerWallet", FILTER_SANITIZE_STRING))));
                        $re = '/^[1-9A-HJ-NP-Za-km-z]{34}$/';
                        preg_match($re, $iWaWi, $matches, PREG_OFFSET_CAPTURE, 0);
                        $cartera = $matches[0][0];
                        if (strlen($cartera) == 34) {


                            $data = array("jsonrpc" => "2.0", "method" => "ticketGen", "id" => "1");
                            $data_string = json_encode($data);

                            $ch = curl_init('http://10.8.0.8:54321/jsonrpc');
                            curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
                            curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);
                            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                            curl_setopt($ch, CURLOPT_HTTPHEADER, array(
                                'Content-Type: application/json',
                                'Content-Length: ' . strlen($data_string))
                            );
                            curl_setopt($ch, CURLOPT_TIMEOUT, 5);
                            curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 5);

                            $response = curl_exec($ch);

                            curl_close($ch);

                            $result = json_decode($response)->result;
                            
                            dbIni();

                            $sql = "INSERT INTO public.txrep(urlhash,wallet,address_rec) VALUES('" .
                                    $result->urlhash . "','$iWaWi','" . $result->address . "');";
                            $query = pg_query($sql) or die('La consulta fallo: ' . pg_last_error());
                            $ins = pg_affected_rows($query);
                            if ($ins == 1) {
                                $Pg = "<p id='msgTx' style='background-color: #00CC00; color: #000000;'>";
                                $Pg .= "<span style='background-color: #888888; color: #00CC00; font-size: x-large;'>( i )</span> ";
                                $Pg .= "<b>Resultados: url_hash='" . $result->urlhash . "' address='" . $result->address . "'</b></p><br>";
                                $Pg .= "<script type=\"text/javascript\" language=\"javascript\">\n//<![CDATA[\n";
                                $Pg .= "window.top.document.location.href=\"/ticket/".$result->urlhash."\";\n";
                                $Pg .= "// ]]>\n</script>\n";
                            }
                            print $Pg;
                        } else {
                            $Pg = "<p id='msgTx' style='background-color: #FF0000; color: #000000;'>";
                            $Pg .= "<span style='background-color: #888888; color: #CC0000; font-size: x-large;'>( i )</span> ";
                            $Pg .= "<b style=\"font-weight: bolder\">Need a valid wallet number.</b></p><br>";
                            print $Pg;
                        }
    /*                } else {
                        $Pg = "<p id='msgTx' style='background-color: #FF0000; color: #000000;'>";
                        $Pg .= "<span style='background-color: #888888; color: #CC0000; font-size: x-large;'>( i )</span> ";
                        $Pg .= "<b style=\"font-weight: bolder\">Need to check the captcha.</b></p><br>";
                        print $Pg;
                    } */
                }
                ?>
                <p id="frMsg"></p>
                <div class="containerf col-sm-12">
                    <div id="frmIncome">
                        <form id="frmInFrm" class="form-horizontal" method="post" action="infr.php">
                            <div class="form-group">
                                <label for="WinnerWallet" class="col-sm-2 control-label">
                                    Wallet if be the winner:
                                </label>
                                <div class="col-sm-10">
                                    <input id="WinnerWallet" name="WinnerWallet" type="text" class="form-control width100" autocomplete="off"  onkeyup="frInR()">
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="g-recaptcha col-sm-2" data-sitekey="6Ldbc1IUAAAAAF8ozFUwjNCFhrTGv35rYdPPc91Z"></div><br>
                            </div>
                            <div class="form-group">
                                <!-- div class="col-sm-2">
                                    <i class="icon-spinner icon-spin icon-large"></i>
                                </div -->
                                <div class="col-sm-10">
                                    <button id="frInButton" data-style="expand-right" class="form-control" onclick="return frInVal();">
                                        Send
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>                
            </article>
        </div>
    </body>
</html>
