// Set the date we're counting down to
//var countDownDate = Date.UTC(2018,4,28,13,25,0);

// Update the count down every 1 second
var x = setInterval(function() {

  // Get todays date and time
  var now = new Date().getTime();

  // Find the distance between now an the count down date
  var distance = countDownDate - now;

  // Time calculations for days, hours, minutes and seconds
  var days = Math.floor(distance / (1000 * 60 * 60 * 24));
  var hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
  var minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
  var seconds = Math.floor((distance % (1000 * 60)) / 1000);

  // Display the result in the element with id="demo"
//  document.getElementById("tcd").innerHTML = ""+days + "d " + hours + "h "   + minutes + "m " + seconds + "s ";

  document.getElementById("days").innerHTML = days;
  document.getElementById("hours").innerHTML = hours;
  document.getElementById("minutes").innerHTML = minutes;
  document.getElementById("seconds").innerHTML = seconds;

  // If the count down is finished, write some text
  if (distance < 0) {
    clearInterval(x);
    document.getElementById("tcd").innerHTML = "EXPIRED";
  }
}, 1000);

function frInVal() {
    var msg = "";
    var ctrl1 = document.getElementById("IncomeTxId");
    var ctrl2 = document.getElementById("WinnerWallet");
    
    if (ctrl1.value.length !== 64) {
       msg = "The value entered has an incorrect length.";
       ctrl1.style.backgroundColor = "#880000";
    }
    if(ctrl2.value.length !== 34) {
       msg = "The value entered has an incorrect length.";
       ctrl2.style.backgroundColor = "#880000";
    }
    if (msg !== ""){
        document.getElementById("frMsg").innerHTML = msg;
        return false;
    } else {
        return true;
    }
}

function frInR(){
    document.getElementById("frMsg").innerHTML="";
    document.getElementById("msgTx").innerHTML="";
    document.getElementById("IncomeTxId").style.backgroundColor = "#08a3c7";
    document.getElementById("WinnerWallet").style.backgroundColor = "#08a3c7";
}